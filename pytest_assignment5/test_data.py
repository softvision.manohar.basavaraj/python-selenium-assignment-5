from selenium.webdriver.common.by import By

# Country DROP DOWN
country_input_name = "Indonesia"

# DROP DOWN OPTION TWO
option_two = "option2"

# ALERT TEXT BOX
input_name = "Manohar"
verify_name = "Manohar,"

# NEW WINDOW TITLE

new_window_title = "QA Click Academy | Selenium,Jmeter,SoapUI,Appium,Database testing,QA Training Academy"

table_data_url = "https://www.rahulshettyacademy.com/AutomationPractice/"

selenium_key_word = "Selenium"