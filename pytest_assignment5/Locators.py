from selenium.webdriver.common.by import By

# Radio Button
RADIO_BUTTON = (By.XPATH, "//input[@value='radio100']")

# Country Drop Down
COUNTRY_DROP_DOWN = (By.XPATH, "//input[@id='autocomplete']")

# Drop Down Options
DROP_DOWN_OPTIONS = (By.XPATH, "//select[contains(@id,'dropdown-class-example')]//option[@value='option2']")

# ALERT_COMPONENT
ALERT_TEXTBOX = (By.XPATH, "//input[contains(@name,'name')]")
ALERT_TRIGGER_BUTTON = (By.XPATH, "//input[contains(@id,'alertbtn')]")

# New Window
OPEN_NEW_WINDOW_BUTTON = (By.XPATH, "//button[contains(@id,'openwindow')]")

# Table data
TABLE_ROW = (By.XPATH, "//table[contains(@id,'product')]//tr")

# Hide Button
HIDE_BUTTON = (By.XPATH, "//input[@id='hide-textbox']")
HIDE_TEXTBOX = (By.XPATH, "//input[@id='displayed-text']")

# Mouse Over
MOUSE_OVER_BUTTON = (By.XPATH, "//button[@id='mousehover']")
MOUSE_OVER_CONTENTS = (By.XPATH, "//div[@class='mouse-hover-content']//a")

# iFrames
ACCESS_FRAMES = (By.TAG_NAME, "iframe")