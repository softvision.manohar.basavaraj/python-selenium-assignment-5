import pytest
from selenium import webdriver

driver = webdriver.Chrome('C:/Users/ADMIN/Documents/chrome/chromedriver.exe')


class BaseClass():

    @pytest.fixture
    def setUp(request):
        # Maximize the Winow
        driver.maximize_window()

        # Launch the Webpage
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/ ")
        request.driver = driver
        yield driver

    @pytest.fixture
    def tearDown(request):
        request.driver = driver
        yield driver
        driver.quit()
