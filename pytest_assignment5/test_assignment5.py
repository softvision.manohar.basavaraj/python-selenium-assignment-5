import pytest
import os, sys, inspect
import logging
import logging.config

logging.config.fileConfig("logging.conf")

# create logger
logger = logging.getLogger("test_case")

# fetch path to the directory in which current file is, from root directory or C:\ (or whatever driver number it is)

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# extract the path to parent directory
parentdir = os.path.dirname(currentdir)
# insert path to the folder from parent directory from which the python module/ file is to be imported
sys.path.insert(0, parentdir)

from pytest_assignment5 import base_class
from pytest_assignment5.rahulshetty_automationpractice_page import RahulShettyAutomationPracticePage
from selenium import webdriver


class TestCase(base_class.BaseClass):

    @pytest.mark.usefixtures("setUp")
    def test_start(self):
        title = self.driver.title
        logger.info('EXECUTION STARTED....')
        self.mainpage = RahulShettyAutomationPracticePage(self.driver)

        # Call Radio Button
        self.mainpage.radio_button()

        # Call for Suggestion Drop Down
        self.mainpage.suggestion_drop_down()

        # Call for Options Drop Down
        self.mainpage.options_drop_down()

        # Call for Alert Component
        self.mainpage.alert_component()

        # Call for New Window Component
        self.mainpage.new_window_component()

        # Call for Read Table Data
        self.mainpage.read_table_data()

        # Call for Hide Component
        self.mainpage.hide_component()

        # Call for Mouseover
        self.mainpage.mouse_over()

        # Call Frame Component
        self.mainpage.frames_component()

    @pytest.mark.usefixtures("tearDown")
    def test_close(self):
        logger.info('EXECUTION COMPLETED!!!')
