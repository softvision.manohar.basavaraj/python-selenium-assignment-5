from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains

from pytest_assignment5 import Locators
from pytest_assignment5 import base_class
from pytest_assignment5 import test_data
from lxml import etree
import urllib.request
from selenium.webdriver.common.by import By
import logging.config

logging.config.fileConfig("logging.conf")

# create logger
logger = logging.getLogger("rahulshetty_automationpractice_page")


class RahulShettyAutomationPracticePage(base_class.BaseClass):

    def __init__(self, driver):
        self.driver = driver

    def radio_button(self):
        logger.info('RADIO_BUTTON COMPONENT STARTED')
        try:
            radio_button_two = self.driver.find_element(*Locators.RADIO_BUTTON)
            self.driver.find_element(*Locators.RADIO_BUTTON).click()
            if radio_button_two.is_selected():
                assert True, 'Radio Button 2 - Not selected'
        except NoSuchElementException as exception:
            logger.error('RADIO BUTTON NOT FOUND, %s', exception.msg)

        logger.info('RADIO BUTTON COMPONENT CLOSED')

    def suggestion_drop_down(self):
        logger.info('SUGGESTION_DROP_DOWN COMPONENT STARTED')
        try:
            country_drop_down = self.driver.find_element(*Locators.COUNTRY_DROP_DOWN)
            self.driver.find_element(*Locators.COUNTRY_DROP_DOWN).send_keys(test_data.country_input_name)
            if country_drop_down.text == test_data.country_input_name:
                assert True, "Indonesia has not been selected"
        except NoSuchElementException as exception:
            logger.error('SUGGESTION_DROP_DOWN NOT FOUND %s', exception.msg)
        logger.info('SUGGESTION_DROP_DOWN COMPONENT COMPLETED')

    def options_drop_down(self):
        logger.info('OPTIONS_DROP_DOWN COMPONENT STARTED')
        try:
            drop_down_options = self.driver.find_element(*Locators.DROP_DOWN_OPTIONS)
            self.driver.find_element(*Locators.DROP_DOWN_OPTIONS).click()
            if drop_down_options == test_data.option_two:
                assert True, "OPTION 2 NOT SELECTED"
        except NoSuchElementException as exception:
            logger.error('OPTIONS_DROP_DOWN NOT FOUND %s', exception.msg)
        logger.info('OPTIONS_DROP_DOWN COMPONENT COMPLETED')

    def alert_component(self):
        logger.info('ALERT COMPONENT STARTED')
        try:
            alter_text_box = self.driver.find_element(*Locators.ALERT_TEXTBOX)
            self.driver.find_element(*Locators.ALERT_TEXTBOX).send_keys(test_data.input_name)
            self.driver.find_element(*Locators.ALERT_TRIGGER_BUTTON).click()
            name_alert_handle = self.driver.switch_to.alert
            check_name_from_alert = name_alert_handle.text
            # Fetching the name from the Alert message and Assert
            my_name = check_name_from_alert.split(" ")
            if my_name[1] == test_data.verify_name:
                name_alert_handle.accept()
                assert True, "Verification of name did not go well"
        except NoSuchElementException as exception:
            logger.error('ALERT COMPONENT NOT FOUND %s', exception.msg)
        logger.info('ALERT COMPONENT COMPLETED')

    def new_window_component(self):
        logger.info('NEW_WINDOW COMPONENT STARTED')
        try:
            # Marking Parent window and #Switching to child window
            parent_window = self.driver.window_handles[0]
            self.driver.find_element(*Locators.OPEN_NEW_WINDOW_BUTTON).click()
            child_window = self.driver.window_handles[1]
            self.driver.switch_to.window(child_window)
            if self.driver.title == test_data.new_window_title:
                assert True
                self.driver.close()
            # Switching back to parent window
            self.driver.switch_to.window(parent_window)
        except NoSuchElementException as exception:
            logger.error('NEW_WINDOW COMPONENT NOT FOUND %s', exception.msg)
        logger.info('NEW_WINDOW COMPONENT COMPLETED')

    def read_table_data(self):
        logger.info('TABLE_DATA COMPONENT STARTED')
        try:
            # Reading Table data using lxml library
            web = urllib.request.urlopen(test_data.table_data_url)
            s = web.read()
            html = etree.HTML(s)

            # Get all 'tr'
            tr_nodes = html.xpath("//table[contains(@id,'product')]//tr")

            # Get text from rest all 'tr'
            td_content = [[td.text for td in tr.xpath('td[2]')] for tr in tr_nodes[1:]]

            # Counter to set the count for Selenium courses
            count_of_selenium_courses = 0
            for gotData in td_content:
                for data in gotData:
                    selenium_word = data.split(" ")
                    if test_data.selenium_key_word in selenium_word:
                        count_of_selenium_courses += 1
            logger.info('TOTAL SELENIUM COURSES ARE: %s', count_of_selenium_courses)
        except NoSuchElementException as exception:
            logger.error('TABLE_DATA COMPONENT NOT FOUND %s', exception.msg)

        logger.info('TABLE_DATA COMPONENT COMPLETED')

    def hide_component(self):
        logger.info('HIDE COMPONENT STARTED')
        try:
            self.driver.find_element(*Locators.HIDE_BUTTON).click()
            textbox = self.driver.find_element(*Locators.HIDE_TEXTBOX)
            if textbox.is_displayed():
                assert True, 'The text box is still visible'
        except NoSuchElementException as exception:
            logger.error('HIDE COMPONENT NOT FOUND %s', exception.msg)
        logger.info('HIDE COMPONENT COMPLETED')

    def mouse_over(self):
        logger.info('MOUSE_OVER COMPONENT STARTED')
        try:
            action = ActionChains(self.driver)
            mouse_over_button = self.driver.find_element(*Locators.MOUSE_OVER_BUTTON)
            action.move_to_element(mouse_over_button).perform()
            mouse_over_data = []
            for options in self.driver.find_elements(*Locators.MOUSE_OVER_CONTENTS):
                option_title = options.text
                mouse_over_data.append(option_title)
        except NoSuchElementException as exception:
            logger.error('MOUSE_OVER COMPONENT NOT FOUND %s', exception.msg)
        logger.info('MOUSE_OVER COMPONENT COMPLETED')

    def frames_component(self):
        logger.info('FRAMES COMPONENT STARTED')
        try:
            # Working with iFrames
            frame = self.driver.find_element(*Locators.ACCESS_FRAMES)
            self.driver.switch_to.frame(frame)
            url_count = 0
            for link in self.driver.find_elements(By.XPATH, "//a"):
                url_count += 1
            logger.info('COUNT of URLs inside frame: %s', url_count)
            self.driver.switch_to.default_content()
        except NoSuchElementException as exception:
            logger.error('FRAMES COMPONENT NOT FOUND %s', exception.msg)
        logger.info('FRAMES COMPONENT COMPLETED')
